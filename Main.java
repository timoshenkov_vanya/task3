package org.example;

public class Main {
    public static void main(String[] args) {
        int firstFib = 0;
        int secondFib = 1;
        int midFib;
        System.out.println("For");
        for (int i = 0; i < 11; i++) {
            System.out.println(secondFib);
            midFib = firstFib;
            firstFib = secondFib;
            secondFib = midFib + secondFib;
        }
        System.out.println("While");
        int firstFibW = 0;
        int secondFibW = 1;
        while (secondFibW != 89 + 55) {
            System.out.println(secondFibW);
            midFib = firstFibW;
            firstFibW = secondFibW;
            secondFibW = midFib + secondFibW;
        }
    }
}